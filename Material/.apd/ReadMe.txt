﻿
これは？
====================

シナリオ[六号室の幽霊船](https://bitbucket.org/k4nagatsuki/cw_theghostshipfromtheno6room)に使用したイメージファイルの元データです。


ライセンス
--------------------

この`ReadMe.txt`を含め、全て[CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.ja)です。
著作権が存在しないかのように自由に取り扱ってください。


使用ソフトウェア
--------------------

ほとんど以下のアプリケーションを使用して作成しました。

 * [AzPainter2](http://hp.vector.co.jp/authors/VA033749/soft/azpainter2.html)
 * [TreeViewGraphics(TVG)](https://sites.google.com/site/treeviewgraphics)


レタッチ内容のメモ
--------------------

### `cc0_map_of_sindia`

TVGで、

 1. アーティスティック「彫刻刀」
 2. 空間フィルタ「グランジ」(適用量80)
 3. カラー調整「ヴィンテージ」
 4. カラー調整「カラー調整」(色相-12・ハイライト-40)


### その他

TVGで、

 1. アーティスティック「シルクスクリーン」(コントラスト+20・彩度-10)

 * `cc0_chat_room.jpg`のみ彩度を+10にしています。
 * 上甲板の背景の星空は、TVGで、模様「スターフィールド」(大きさ2・色相200)で乱数値を2570・2870に設定して作りました。
 * 雨の背景は、TVGで、特殊効果「雨●」(長さ32・角度1・濃霧フィルタ無し)を入れました。


参考資料
--------------------

帆船の外観や内部を描くにあたって以下のWebページを参考にしました。ありがとうございました。
今はこうした資料を手軽に検索して観る事ができます。とてもよい時代です。

 * [あれ？誰だっけ？ - 【船の各部名称】船体](http://www53.atwiki.jp/alonsodeleyva/pages/57.html)
 * [航海士便り - 帆船のひみつ](http://homepage2.nifty.com/go_tokyo/kaiwo_walk.htm)
 * [ど～も趣味ましぇん - 憧れの帆船模型](http://www.geocities.jp/ilikehobby/model/seiling.html)
 * [Wikipediaの帆船に関するいくつかの記事](https://ja.wikipedia.org/wiki/%E5%B8%86%E8%88%B9)
 * `帆船`や`ブリッグ`で画像検索すると出てくる画像の数々
 * その他メモしていない様々なWebページ

天井が高いとか綱が足りないとか構造や積荷がおかしいとかドアが開く向きがとか色々あるのですが、ファンタジー的ご都合主義という事でお許しください。
