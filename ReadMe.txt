
六号室の幽霊船 Version.3
======================================================================

概要
------------------------------------------------------------

対象世界観       : 中世ファンタジー
対象レベル       : 2～4
ジャンル         : 探索
前提条件         : 特に無し
プレイ時間       : 30分程度
データバージョン : Wsn.2 (CardWirthPy 2以上)
公開リポジトリ   : https://bitbucket.org/k4nagatsuki/cw_theghostshipfromtheno6room

魔術師学連「賢者の塔」の准導師からの依頼。所は「塔」が所有する寮の一室。
ある魔術師がその部屋に引き籠ってしまったため、なんとかして引き摺り出す仕事だというのだが……。

---

CardWirthPy 2以上向けの追加シナリオです。CardWirthPyは次のサイトから入手できます。
 * [CardWirthPy Reboot](https://bitbucket.org/k4nagatsuki/cardwirthpy-reboot)

編集にはCWXEditor 5のβ版を使用しました。
 * [CWXEditor](https://bitbucket.org/k4nagatsuki/cwxeditor)

---

前提条件無しではありますが、そろそろ駆け出しは卒業したかな、という辺りの冒険者を想定しています。
標準以外の対応キーコードは、`魔力感知`・`生命感知`・`交渉`・`魅了`・`透明`です。特に`魔力感知`は有効に働く場面が多いでしょう。


更新履歴
------------------------------------------------------------

### 2017年8月6日 Version.3

 * 一部文言を変更(「錆無し鉄」→「不錆鉄」)。
 * 共闘の条件を満たした時は下甲板の戦闘でより有利になるようにした。

### 2016年11月27日 Version.2 (Wsn.2対応化)

 * 各カードイメージを2倍スケール版を追加。
 * 各背景イメージを2倍スケールにし、1倍スケールでは一部イメージをスムージングして表示するようにした。

### 2016年8月15日 Version.1.1

 * メッセージや称号の誤りの修正(「魔術士学連」→「魔術師学連」)。
 * 意図が分かりやすいように一部メッセージを手入れ。

### 2016年8月1日 Version.1

公開。


各ファイルの著作権とライセンス
------------------------------------------------------------

シナリオのアーカイブ全体を再配布などする場合は、[CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en)ライセンスの下で配布されているファイルが含まれている事に注意してください。

以下の著作権情報を削らなければ大方問題はありませんし、音声素材を加工した場合はそれを明記すれば大丈夫です。


### シナリオの作者が作成したファイル及びキャラクター・設定など

全て[CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.ja)の下に配布します。これらのファイルは、いかなる条件も無く誰でも自由に使用する事ができます。
以下、`*`はワイルドカードです。任意のファイル名に読み替えてください。

 * `*.sli`は、BGMなどの音声を適切にループ再生するために[FOLE - FICUSEL Ogg Loop Editor](http://ficusel.com/?pagereq=prod&prodreq=fole)を使用して作成しました。
 * `cc0_*.png`及び`cc0_*.jpg`は[AzPainter2](http://hp.vector.co.jp/authors/VA033749/soft/azpainter2.html)をはじめとするいくつかのアプリケーションを使用して作成しました。
 * `*.xml`は[CWXEditor](https://bitbucket.org/k4nagatsuki/cwxeditor)を使用して作成したシナリオのデータファイルです。これもCC0で配布しますので、自由にご利用ください。
 * このファイル`ReadMe.txt`自体もCC0で配布する事を念のために記しておきます。


### [Audionautix](http://audionautix.com/)より

全て[CC-BY 3.0](http://creativecommons.org/licenses/by/3.0/deed.en_US)ライセンスの下に配布されています。
また、これらのファイルはMP3からOgg Vorbis形式に変換・再サンプリングしています。

 * Jason Shaw様より:
    * BootsBootsBoots.ogg
    * Crushin.ogg
    * DatGrooveFullTrack.ogg
    * Drifting2.ogg
    * Periscope.ogg
    * Pioneers.ogg
    * RP-FightScene_modified.ogg (末尾部分を編集しています)


### [SoundBible.com](http://soundbible.com/)より

ライセンスが「Attribution 3.0」となっているファイルは[CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en)ライセンスの下に配布されています。
ライセンスが「Public Domain」となっているファイルはパブリックドメイン(著作権無し)です。
また、これらのファイルはWAVからOgg Vorbis形式に変換・再サンプリングしています。

 * Marianne Gagnon様より:
    * Button_Press_4-Marianne_Gagnon-570460555_modified.ogg (Attribution 3.0 / 末尾に空白時間を追加しています)
 * Mike Koenig様より:
    * Earthquake-SoundBible.com-768042906_modified.ogg (Attribution 3.0 / 音量を調節しています)
    * Rain_Background-Mike_Koenig-1681389445.ogg (Attribution 3.0)
    * Footsteps-SoundBible.com-534261997_modified.ogg (Attribution 3.0 / 音量・ピッチを調節しトリミングしています)
    * Footsteps-SoundBible.com-534261997_modified_2.ogg (Attribution 3.0 / 音量・ピッチを調節しトリミングしています)
    * Splitting Wood-SoundBible.com-472830800_modified.ogg (Attribution 3.0 / エフェクトの追加・音量調節・トリミングなどの加工を行っています)
    * Thunder-Mike_Koenig-315681025.ogg (Attribution 3.0)
 * Ploor様より:
    * Video_Game_Splash-Ploor-699235037_modified.ogg (Public Domain / 音量を調節しています)
 * Pwlae様より:
    * Rain-Pwlae-390675481_modified.ogg (Attribution 3.0 / 音量を調節しています)


余談
------------------------------------------------------------

WSN形式のシナリオは、CWXEditorを使ってCardWirth 1.28～1.50向けに移植する事ができます。以下のようにするとよいでしょう。

 1. 対象のシナリオをCWXEditorで開く。
 2. 「名前を付けて保存」で「クラシックシナリオ」に変換して保存する。
 3. 「検索と置換」で「誤り検索」を行い、互換性の無い箇所の一覧を得る。
 4. 互換性の無い箇所を個別に修正する。

CardWirth 1.28～1.30へ向けて移植する場合は、CWXEditorの「設定」>「その他」>「対象エンジン」で当該エンジンを指定しておきましょう。

なお、この「六号室の幽霊船」は、Wsn.1の機能の紹介も兼ねているので、Wsn.1で追加された新要素をかなり大量に使っています。このシナリオの移植は骨が折れるはずです。

逆に、WSN形式固有の要素をあまり使っていないシナリオであれば、移植は簡単です。
